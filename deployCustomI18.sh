#!/bin/bash

#This is source OpenIAM proxy version
export OPENIAM_VERSION="4.2.1.2"
#This is the Tag of the source image
export OPENIAM_TAG="qa"
#This is the name of image that will contain your customization.
#This image name you should setup in the config for kubernetes for example or in the docker-compose.yaml file
export OUTPUT_IMAGE_NAME="openiamdocker/ui:custom"

docker build --build-arg OPENIAM_VERSION="${OPENIAM_VERSION}" --build-arg OPENIAM_TAG="${OPENIAM_TAG}" ./  -t ${OUTPUT_IMAGE_NAME} -f Dockerfile-ui
