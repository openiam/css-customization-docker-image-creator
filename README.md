# Build containers images with customization

## Build apache2 module with custom CSS file for UI Theme usage.

1. Open _deployCustomCSS.sh_ file and change values for variable
```bash
  #This is source OpenIAM proxy version
  export OPENIAM_VERSION="4.2.1.2"
  #This is the Tag of the source image
  export OPENIAM_TAG="qa"
  #This is the name of image that will contain your customization.
  #This image name you should setup in the config for kubernetes for example or in the docker-compose.yaml file
  export OUTPUT_IMAGE_NAME="rproxy:custom"
```  
2. Copy your CSS files with customization to folder _css/_ . 
   >Files _customization1.css_ and _customization2.css_ are just example. if you don't use them. please delete it at this step. 
3. Run _./deployCustomCSS.sh_ script.
   1. Use this image for your installation. For example in case of docker swarm usage, please do changes in files:
       [rproxy/docker-compose.yaml](https://bitbucket.org/openiam/openiam-docker-compose/src/RELEASE-4.2.1.2/3.2/rproxy/docker-compose.yaml)
   or [rproxy-ssl/docker-compose.yaml](https://bitbucket.org/openiam/openiam-docker-compose/src/RELEASE-4.2.1.2/3.2/rproxy-ssl/docker-compose.yaml)
   from:

   > image: "openiamdocker/rproxy:debian-${OPENIAM_VERSION_NUMBER}-${BUILD_ENVIRONMENT}"

   to:

   > image: "rproxy:custom"

and redeploy the rproxy (with startup.sh for example)

5. if you are using the kubernetes then you should change image name in the openiam-rproxy helm chart [here](https://bitbucket.org/openiam/kubernetes-docker-configuration/src/248a9a3991e7589c9438ad668fba42cfe02f4384/openiam-rproxy/templates/rproxy-statefulset.yaml#lines-60)
6. When your customized images will run and container/pod will become healthy you will be able to access your custom CSS 
files as _http://<server_name>/static/<YOUR_CUSTOM_FILE_NAME>.css>_. For example as _http://localhost/static/customization1.css_. 
This URL you should use at UI Theme configuration page. 

## Build ui module with custom internationalization properties (labels).

1. Open _deployCustomI18.sh_ file and change values for variable
```bash
  #This is source OpenIAM proxy version
  export OPENIAM_VERSION="4.2.1.2"
  #This is the Tag of the source image
  export OPENIAM_TAG="qa"
  #This is the name of image that will contain your customization.
  #This image name you should setup in the config for kubernetes for example or in the docker-compose.yaml file
  export OUTPUT_IMAGE_NAME="ui:custom"
```  

2. Modify files in _messages/_ folder the full